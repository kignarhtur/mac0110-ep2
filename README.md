# MAC0110-EP2

Will possui N cartas contendo dı́gitos de 0 a 9. Dado um conjunto de cartas, verifique se é possı́vel formar os números pedidos.

Parte 1:
    Dadas N cartas, é possı́vel formar o número preferido de Will?
        Crie a função checkWill(cards)

Parte 2:
    Will consegue formar o número preferido do amigo dele e o seu  número preferido com as cartas dadas?
        Crie a função checkTaki(cards)

Parte 3:
    Will consegue formar o seu próprio número, o número do seu amigo e o número do seu pai com as cartas que tem?
        Crie a função checkJackson(x, cards)

Parte 4:
    Will consegue representar seu número preferido, com as cartas que tem, na base B, em que B é dado e B <= 10?
        Crie a função checkWillBase(b, cards)

Parte Bônus:
    Will junta os números preferidos de vários amigos. Dados esses números e N cartas, é possı́vel formar os número preferido de Will e dos seus amigos?
        Crie a função checkFriends(numbers, cards)
